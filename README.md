# README

This is a Rails app built to serve as my online home-base.  The focus of the site is my blog, where I log my coding adventures.  I also keep an updated "about" page and resumé up on the site.

The site is hosted on Heroku, and you can find it at [toneeyoung.com](http://www.toneeyoung.com/).

Shouts out to [this book by Michael Hartl](https://www.railstutorial.org/) and this [video by Mackenzie Child](https://www.youtube.com/watch?v=BI_VnnOLSKY&list=PL23ZvcdS3XPLNdRYB_QyomQsShx59tpc-&index=2) for helping me get this far in my Rails journey!
