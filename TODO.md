### Changes to implement in future versions of Blog 

##### Improve my testing skills
- [ ] Write model and controller tests 
- [ ] Write integration tests 

##### Familiarize myself with ActionMailer 
- [ ] Send email notifications to myself when someone leaves a comment on a post 

##### Improve security
- [ ] Implement SSL certification to use HTTPS 

##### Improve user experience
- [ ] Create portfolio page so visitors don't have to leave the site to see projects 

